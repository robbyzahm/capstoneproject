﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capstone_PhoneApp.PathEngine
{
    public class Grid
    {
        private int[,] grid;
        private int col;
        private int row;
        public Grid(int gridSizeX, int gridSizeY)
        {
            this.col = gridSizeX;
            this.row = gridSizeY;
        }

        public void MakeGrid()
        {
            grid = new int[col, row];
            for (int x = 0; x < col; x++)
            {
                for (int y = 0; y < row; y++)
                {
                    grid[x, y] = 1;
                }
            }
        }

        public void SetUpObsticals(List<PathData> obsticalCoordinates)
        {
            for(int i =0; i < obsticalCoordinates.Capacity;i++)
            {
                PathData coordinates = obsticalCoordinates[i];
                grid[coordinates.x, coordinates.y] = 3;
            }
        }
    }
}
