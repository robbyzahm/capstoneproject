﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Capstone_PhoneApp.Core_Classes;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Threading;
//using Capstone_PhoneApp.PathEngine;
//using Capstone_PhoneApp.Core_Classes.Path_Classes;
namespace Capstone_PhoneApp.Pages
{
    public partial class DisplayPath : PhoneApplicationPage
    {
        private static int arrayWidthX = 6;
        private static int arrayHeightY = 9;
        private Rectangle rec;
        private Rectangle[,] arrayRec;
        private int margin1 = 0;
        private int margin2 = 0;
        private int margin3 = 400;
        private int margin4 = 617;
        //private CreateGrid grid = new CreateGrid(arrayWidthX, arrayHeightY);
        //Random rand = new Random();
        public DisplayPath()
        {
            InitializeComponent();

            //MyColors color = new MyColors();
            //color.color1 = new SolidColorBrush(Colors.Yellow);
            //box.DataContext = color;
            Rectangle[,] tarrayRec = (Rectangle[,])PhoneApplicationService.Current.State["arrayRec"];
            arrayRec = tarrayRec;

            for (int y = 0; y < arrayHeightY; y++)
            {
                for (int x = 0; x < arrayWidthX; x++)
                {
                    if( tarrayRec[x, y] != null)
                    {
                        rec = tarrayRec[x, y];
                        //LayoutRoot.Children.Add(rec);
                    }
                }
            }
            //InitializeGrid();
            //CreateUI();
            //AddObsticals();
            //UpdateGridView();
        }
        
        public void InitializeGrid()
        {
            for (int y = 0; y < arrayHeightY; y++)
            {
                margin1 = 0;
                margin3 = 400;
                for (int x = 0; x < arrayWidthX; x++)
                {
                    rec = new Rectangle();
                    rec.Height = 80;
                    rec.Width = 80;
                    rec.Fill = new SolidColorBrush(Colors.Green);
                    rec.Margin = new Thickness(margin1, margin2, margin3, margin4);
                    arrayRec[x, y] = rec;
                    //grid.MakeGrid();//area mowable but not mowed
                    //LayoutRoot.Children.Add(rec);
                   
                    margin1 += 80;
                    margin3 -= 80;
                }
                if (margin2 > 640)
                    margin2 = 0;
                else
                    margin2 += 80;
                if (margin4 < 0)
                    margin4 = 688;
                else
                    margin4 -= 80;
            } 
        }

        public void CreateUI()
        {
            Rectangle rec;
            for (int y = 0; y < arrayHeightY; y++)
            {
                for (int x = 0; x < arrayWidthX; x++)
                {
                    if (arrayRec[x, y] != null)
                    {
                        rec = arrayRec[x, y];
                        //LayoutRoot.Children.Add(rec);
                    }
                }
            }
        }
        /*
       List<PathData> pathdata = new List<PathData>();
       PathData data;
       public void AddObsticals()
       {
           data = new PathData(0,0);
           pathdata.Add(data);
            
           data = new PathData(3, 4);
           pathdata.Add(data);
           data = new PathData(4, 3);
           pathdata.Add(data);
           data = new PathData(4, 4);
           pathdata.Add(data);

           //grid.SetUpObsticals(pathdata);
       }
       
       public void UpdateGridView()
       {
           SolidColorBrush obstical = new SolidColorBrush(Colors.Red);
           Rectangle rec;
           Movement move = new Movement(arrayRec);
           for (int y = 0; y < 9; y++)
           {
               for (int x = 0; x < 6; x++)
               {
                   //LayoutRoot.Children.Clear();
                   if (arrayRec[x, y] != null)
                   {
                       //if (arrayRec[x, y] == arrayRec[3, 3])
                       for(int i =0; i<pathdata.Count();i++)
                       {
                           int tempx = pathdata[i].x;
                           int tempy = pathdata[i].y;
                           if(x == tempx && y == tempy)
                           {
                               Main helperMethod = new Main();
                               helperMethod.MakeGrid();
                               helperMethod.FindPath();
                                
                           
                               rec = arrayRec[x, y];
                               rec.Fill = new SolidColorBrush(Colors.Red);
                               LayoutRoot.Children.Remove(rec);
                               LayoutRoot.Children.Add(rec);
                           }
                           else
                           {
                               rec = arrayRec[x, y];
                               //LayoutRoot.Children.Remove(rec);
                               rec.Fill = new SolidColorBrush(Colors.Green);

                               //Thread.Sleep(1000);
                               LayoutRoot.Children.Remove(rec);
                               LayoutRoot.Children.Add(rec);
                           }
                       }
                   }
               }
           }
           for (int i = 0; i < pathdata.Count(); i++)
           {
               int tempx = pathdata[i].x;
               int tempy = pathdata[i].y;
            
                   rec = arrayRec[tempx, tempy];
                   rec.Fill = new SolidColorBrush(Colors.Red);
                   LayoutRoot.Children.Remove(rec);
                   LayoutRoot.Children.Add(rec);
                
           }
           /*
           rec = arrayRec[3, 3];
           rec.Fill = new SolidColorBrush(Colors.Red);
           rec = arrayRec[3, 4];
           rec.Fill = new SolidColorBrush(Colors.Red);
           rec = arrayRec[4, 3];
           rec.Fill = new SolidColorBrush(Colors.Red);
           rec = arrayRec[4, 4];
           rec.Fill = new SolidColorBrush(Colors.Red);
             
       }
       
        int count = 0;
        public Color RandomColor()
        {
            Color color = Colors.White;
            if(count == 0)
            {
                color = Colors.Yellow;
                count++;
            }
            else if(count == 1)
            {
                color = Colors.Red;
                count++;
            }
            else if(count == 2)
            {
                color = Colors.Green;
                count++;
            }
            else if(count == 3)
            {
                color = Colors.Blue;
                count++;
            }
            else if(count ==4)
            {
                color = Colors.Brown;
                count = 0;
            }
            return color;
        }
         * */
    }
}