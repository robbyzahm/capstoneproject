﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Capstone_PhoneApp.Pages
{
    public class PathFinding
    {
        private SolidColorBrush red = new SolidColorBrush(Colors.Red);
        private SolidColorBrush yellow = new SolidColorBrush(Colors.Yellow);
        private SolidColorBrush green = new SolidColorBrush(Colors.Green);
        private SolidColorBrush gray = new SolidColorBrush(Colors.Gray);
        private LawnCreationPart2 lawnCreationPart2;
        private Rectangle[,] arrayRec;
        private Rectangle rec;
        private int arrayHeightY;
        private int arrayWidthX;
        private int currentY;
        private int currentX;

        //rows efficeny
        private int NodeCountRows;
        private int turnCountRows;
        private int NodeRetraveredRows;

        //spirals efficency
        private int NodeCountSpirals;
        private int turnCountSpirals;
        private int NodeRetraveredSpirals;

        private Rectangle[,] rows;
        private Rectangle[,] spirals;

        public PathFinding(LawnCreationPart2 lawnCreationPart2, Rectangle[,] arrayRec, int width, int height)
        {
            // TODO: Complete member initialization
            this.lawnCreationPart2 = lawnCreationPart2;
            this.arrayHeightY = height;
            this.arrayWidthX = width;
            this.arrayRec = arrayRec;
            rows = new Rectangle[arrayWidthX, arrayHeightY];
            spirals = new Rectangle[arrayWidthX, arrayHeightY];
        }
        
        
        
        public void UpDateUIByRows()
        {
            int x = 0;
            int y = 0;
            //bool mowableAreaFound = false;

            Boolean moveRight = true;
            Boolean moveDown = true;
            //Boolean circleProperty = true;
            Boolean mowableAreaFound = true;
            while (mowableAreaFound)
            {
                rows = arrayRec;
                rec = rows[x, y];
                rec.Dispatcher.BeginInvoke(new Action(()=>
                {
                    SolidColorBrush rectBrush = (SolidColorBrush)rec.Fill;

                    int tempx = x;
                    int tempy = y;
                    if (arrayWidthX >= arrayHeightY)//if width is greater go right
                    {
                        if (moveRight)
                        {
                            tempx++;
                            if (tempx <= arrayWidthX)
                            {
                                //if(circleProperty)
                                
                                    //Circle(x,y);
                                    //circleProperty = false;
                                
                                if (rectBrush.Color == Colors.Green)//current rectangle is green set it to yellow and find next rectangle
                                {
                                    //rec.Fill = new SolidColorBrush(Colors.Yellow);
                                    rows[x, y].Fill = yellow;
                                    //if (rows[tempx, tempy].Fill == red)//next rectangle is red - an obstical
                                    {
                                        //dont go forward and find another mowable square or the next available unmowed square
                                        /*FindNextMowableSquare();
                                        if(rows[tempx--,tempy] != null)//
                                        {
                                            x--;
                                            NodeCountRows += 1;
                                            rows[x, y] = rec;
                                        }
                                        */
                                        
                                    }
                                    //else if (rows[tempx, tempy].Fill == yellow)
                                    {
                                        //area is already mowed find next area
                                        //FindNextMowableSquare();
                                        
                                    }
                                    //else if (rows[tempx, tempy].Fill == gray)
                                    {
                                        //area cannot be mowed but can be traversed
                                        
                                    }
                                    //else //its green
                                    if(x >= arrayWidthX && y >= arrayHeightY)//out of bounds
                                    {
                                        mowableAreaFound = false;
                                    }
                                    else
                                    {
                                        x++;
                                        NodeCountRows += 1;
                                    }
                                }
                            }
                            else if (tempx >= arrayWidthX)//turn
                            {
                                
                                //y++;
                                rows[x, y].Fill = yellow;
                                moveRight = false;
                                NodeCountRows += 1;
                                turnCountRows += 1;
                            }
                        }
                        else //move left
                        {
                            tempx--;
                            if (tempx >= 0)
                            {
                                rows[x, y].Fill = yellow;
                                x--;
                                NodeCountRows += 1;
                                turnCountRows += 1;
                            }
                            else if (tempx <= 0)
                            {
                                y++;
                                NodeCountRows += 1;
                                turnCountRows += 1;
                                rows[x, y].Fill = yellow;
                                moveRight = true;
                            }
                        }
                    }
                    else
                    {
                        if (moveDown)//move down
                        {
                            tempy++;
                            if (tempy < arrayHeightY)
                            {
                                y++;
                                NodeCountRows += 1;
                            }
                            else if (tempy >= arrayHeightY)
                            {
                                x++;
                                NodeCountRows += 1;
                                turnCountRows += 1;
                                moveDown = false;
                            }
                        }
                        else //move up
                        {
                            tempy--;
                            if (tempy >= 0)
                            {
                                NodeCountRows += 1;
                                y--;
                            }
                            else if (tempy <= 0)
                            {
                                x++;
                                NodeCountRows += 1;
                                turnCountRows += 1;
                                moveDown = true;
                            }
                        }
                    }
                    for (int i = 0; i < arrayHeightY; i++)
                    {
                        for (int j = 0; j < arrayWidthX; j++)
                        {
                            if(rows[i,j] != null)
                            {
                                rec = rows[i, j];

                                SolidColorBrush brush = (SolidColorBrush)rec.Fill;
                                if (brush.Color == Colors.Green)
                                {
                                    mowableAreaFound = true;
                                    break;
                                }
                                else
                                    mowableAreaFound = false;
                            }
                        }
                        if (mowableAreaFound)
                            break;
                        else
                            mowableAreaFound = false;
                    }
                }));
            }
        }

       
        private void FindNextMowableSquare()
        {
            //Rectangle currentRec;
            Boolean foundNext = false;
            while(!foundNext)
            {
                
            }
        }
        public Boolean MowableAreaFound()
        {
            bool mowableAreaFound = false;
            for (int y = 0; y < arrayHeightY; y++)
            {
                for (int x = 0; x < arrayWidthX; x++)
                {
                    rec = arrayRec[x, y];
                    
                    SolidColorBrush brush = (SolidColorBrush)rec.Fill;
                    if (brush.Color == Colors.Green)
                    {
                        mowableAreaFound = true;
                    }
                }
            }
            return mowableAreaFound;
        }

        public void FindMowableArea(int currentX, int currentY)//check for the next available mowable area
        {
            int tempX = currentX;
            int tempY = currentY;
            if (arrayRec[tempX++, tempY++].Fill == green)//southeast
            {
                currentX = tempX;
                currentY = tempX;
            }
            else if (arrayRec[tempX--, tempY++].Fill == green)//southwest
            {
                currentX = tempX;
                currentY = tempX;
            }
            else if (arrayRec[tempX--, tempY--].Fill == green)//northwest
            {
                currentX = tempX;
                currentY = tempX;
            }
            else if (arrayRec[tempX++, tempY--].Fill == green)//northeast
            {
                currentX = tempX;
                currentY = tempX;
            }
            else if (arrayRec[tempX++, tempY].Fill == green)//east
            {
                currentX = tempX;
                currentY = tempX;
            }
            else if (arrayRec[tempX, tempY++].Fill == green)//south
            {
                currentX = tempX;
                currentY = tempX;
            }
            else if (arrayRec[tempX--, tempY].Fill == green)//west
            {
                currentX = tempX;
                currentY = tempX;
            }
            else if (arrayRec[tempX, tempY--].Fill == green)//north
            {
                currentX = tempX;
                currentY = tempX;
            }
            else //no mowable area around
            {
                CircleObstacles();
            }
        }

        public void CircleObstacles()
        {
            for (int i = 0; i < arrayHeightY; i++)
            {
                for (int j = 0; j < arrayWidthX; j++)
                {
                    if (arrayRec[i, j].Fill == green)//check sides of the obstical and mow around it
                    {
                        //check bottom, top, left, right, four diagnal corners
                    }
                }
            }
        }


        SolidColorBrush recColor;
        public void CircleRight()
        {
            int startx = 0;
            int starty = 0;
            Boolean finishedCircling = false;
            Boolean goDown = false;
            Boolean goRight = true;
            Boolean moveVertical = true;
            //Boolean hitObstical = false;
            int x = startx;
            int y = starty;
            while (!finishedCircling)
            {
                rec = arrayRec[x, y];
                rec.Dispatcher.BeginInvoke(new Action(() =>
                {
                    int tempx = x;
                    int tempy = y;

                    rec.Fill = yellow;//set current node yellow
                    if (moveVertical)
                    {
                        if (goRight == true)
                        {
                            tempx++;//check next rectangle
                            if (tempx < arrayWidthX)//check right bounds
                            {
                                recColor = (SolidColorBrush)arrayRec[tempx, tempy].Fill;
                                Color color = recColor.Color;
                                /*
                                if (color == red.Color)//next place is impassable
                                {
                                    hitObstical = true;
                                    tempx = x;
                                    if (arrayRec[tempx, tempy++].Fill != red && arrayRec[tempx, tempy] != null)
                                    {
                                        x = tempx;
                                        y = tempy;
                                    }
                                    else if (arrayRec[tempx, tempy - 2].Fill != red)
                                    {
                                        x = tempx;
                                        y = tempy;
                                    }
                                }
                                 * */
                                if(color == yellow.Color)
                                {
                                    y++;
                                    rec.Fill = yellow;
                                    moveVertical = false;
                                    goDown = true;
                                }
                                else//color is green
                                {
                                    x++;
                                    //hitObstical = false;
                                    rec.Fill = yellow;
                                    //moveVertical = false;
                                }
                                
                            }
                            else
                            {
                                goDown = true;
                                moveVertical = false;
                            }
                        }
                        else//go left
                        {
                            tempx--;//check next rectangle
                            if (tempx >= 0)//check right bounds
                            {
                                if(arrayRec[tempx,tempy].Fill == yellow)
                                {
                                    rec.Fill = yellow;
                                    moveVertical = false;
                                    goDown = false;
                                }
                                else
                                {
                                    rec.Fill = yellow;
                                    x--;
                                }
                            }
                            else
                            {
                                goDown = false;
                                moveVertical = false;
                            }
                        }
                    }
                    else//go up or down
                    {
                        if (goDown)//go down
                        {
                            tempy++;//check next rectangle
                            if (tempy < arrayHeightY)//check right bounds
                            {
                                rec.Fill = yellow;
                               
                                if (arrayRec[tempx, tempy].Fill == yellow)
                                {
                                    moveVertical = true;
                                    goRight = false;
                                }
                                else
                                {
                                    y++;
                                }
                            }
                            else//next node is either a yellow or off the map
                            {
                                goRight = false;
                                moveVertical = true;
                            }
                        }
                        else//go up
                        {
                            tempy--;
                            rec.Fill = yellow;
                            if (tempy >= 0)//check right bounds
                            {
                                if(arrayRec[tempx,tempy].Fill == yellow)
                                {
                                    moveVertical = true;
                                    goRight = true;
                                }
                                else
                                {
                                    y--;
                                }
                            }
                            else
                            {
                                moveVertical = true;
                                goRight = true;
                            }
                        }
                    }
                }));
                Thread.Sleep(500);
            }
        }
        /*if (hitObstical)
                               {
                                   if (goRight)
                                   {
                                       tempy--;
                                       if (arrayRec[tempx, tempy].Fill == red)
                                       {
                                           x++;
                                       }
                                       else if (arrayRec[tempx, tempy].Fill == green)
                                       {
                                           x = tempx;
                                           y = tempy;
                                           tempy--;
                                           if (arrayRec[tempx, tempy] == null)
                                           {
                                               hitObstical = false;
                                           }

                                       }
                                   }
                                   hitObstical = false;
                               }
                               else
                                * */
    }
}
