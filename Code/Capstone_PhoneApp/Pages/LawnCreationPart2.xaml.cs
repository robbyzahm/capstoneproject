﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Media;
using Capstone_PhoneApp.ViewModels;
using System.Threading;
using Capstone_PhoneApp.PathEngine;
using System.Threading.Tasks;

namespace Capstone_PhoneApp.Pages
{
    //Colors legend
    //red = not mowable obstical
    //green = mowable not mowed grass
    //yellow = mowed grass  
    //gray = obstical that can be traversed
    public partial class LawnCreationPart2 : PhoneApplicationPage
    {
        private SolidColorBrush red = new SolidColorBrush(Colors.Red);
        private SolidColorBrush yellow = new SolidColorBrush(Colors.Yellow);
        private SolidColorBrush green = new SolidColorBrush(Colors.Green);
        private SolidColorBrush gray = new SolidColorBrush(Colors.Gray);

        //private double[] preXArray = new double[10];
        //private double[] preYArray = new double[10];
        public static int arrayWidthX { get; set; }
        public static int arrayHeightY { get; set; }
        private Rectangle rec;
        public Rectangle[,] arrayRec { get; set; }
        private int margin1 = 0;
        private int margin2 = 0;
        private int margin3 = 400;
        private int margin4 = 617;

        private int NodeCountRows;
        private int turnCountRows;
        private int NodeRetraveredRows;

        //spirals efficency
        private int NodeCountSpirals;
        private int turnCountSpirals;
        private int NodeRetraveredSpirals;

        private Rectangle[,] rows;
        private Rectangle[,] spirals;

        public LawnCreationPart2()
        {
            InitializeComponent();
            DimensionsViewModel viewModel = (DimensionsViewModel)PhoneApplicationService.Current.State["yourparam"];
            arrayWidthX= viewModel.lawnWidth;
            arrayHeightY = viewModel.lawnHeight;

            InitializeGrid();
            CreateUI();
            stackPanelGrid.Height = (arrayHeightY * 80)*1.2;
            stackPanelGrid.Width = (arrayWidthX * 80)*1.2;
            stackPanelGrid.Margin = new Thickness(0, 0, 0, 0);
           
            //Touch.FrameReported += new TouchFrameEventHandler(Touch_FrameReported);
        }

        void tap_Frame(object sender, GestureEventArgs e)
        {
            var test = (Rectangle)e.OriginalSource;
            Rectangle rects = test;
            Rectangle rect = null;
            for (int y = 0; y < arrayHeightY; y++)
            {
                for (int x = 0; x < arrayWidthX; x++)
                {
                    if (rects == arrayRec[x, y])
                        rect = arrayRec[x, y];
                }
            }
            if (rect != null)
            {
                //_rects.Add(point.TouchDevice.Id, ri);
                Brush green = new SolidColorBrush(Colors.Green);
                Brush yellow = new SolidColorBrush(Colors.Yellow);
                Brush red = new SolidColorBrush(Colors.Red);
                Brush brush = new SolidColorBrush(Colors.Gray);
                Brush testYellow = new SolidColorBrush(Colors.Yellow);


                SolidColorBrush rectBrush = (SolidColorBrush)rect.Fill;


                if (rectBrush.Color == Colors.Green)
                    rect.Fill = red;
                else if (rectBrush.Color == Colors.Red)
                {
                    rect.Fill = brush;
                }
                else if (rectBrush.Color == Colors.Gray)
                    rect.Fill = yellow;
                else
                {
                    rect.Fill = green;
                }
            }
            rec = rect;
        }

        public void InitializeGrid()
        {//xright,ybottom,xleft,ytop
            int width =80;
            int height = 80;
            int maxGridSize= arrayWidthX * 80;
            margin2 = 0;
            //find top left
            //margin1 = (arrayWidthX * 80) / 2 * -1;
            int minWidth = (arrayWidthX * 80) / 2 * -1;
            int maxWidth = (int)(((arrayWidthX * 80)/2)*2);
            int minHeight = (arrayHeightY * 80) / 2 * -1;
            int maxHeight = (int)((arrayHeightY * 80));
            margin3 = maxWidth;
            margin4 = maxHeight;
            margin1 = 0;
            margin2 = 0;
            arrayRec = new Rectangle[arrayWidthX, arrayHeightY];
            for (int y = 0; y < arrayHeightY; y++)
            {
                for (int x = 0; x < arrayWidthX; x++)
                {
                    rec = new Rectangle();
                    rec.Height = height;
                    rec.Width = width;
                    rec.Tap += tap_Frame;
                    rec.Fill = new SolidColorBrush(Colors.Green);
                    rec.Margin = new Thickness(margin1, margin2, margin3,margin4);
                    arrayRec[x, y] = rec;

                    margin1 += width;
                    margin3 -= 80;
                }
                margin1 = 0;
                margin3 = maxWidth;
                margin2 += 80;
                margin4 -= 80;
            }
        }

        public void CreateUI()
        {
            Rectangle rec;
            for (int y = 0; y < arrayHeightY; y++)
            {
                for (int x = 0; x < arrayWidthX; x++)
                {
                    if (arrayRec[x, y] != null)
                    {
                        rec = arrayRec[x, y];
                        stackPanelGrid.Children.Add(rec);
                        stackPanelGrid.UpdateLayout();
                    }
                }
            }
        }
        private void New_Click(object sender, EventArgs e)
        {
            Clear();
            Rectangle rec;
            for (int y = 0; y < arrayHeightY; y++)
            {
                for (int x = 0; x < arrayWidthX; x++)
                {
                    if (arrayRec[x, y] != null)
                    {
                        rec = arrayRec[x, y];
                        rec.Fill = new SolidColorBrush(Colors.Green);
                        stackPanelGrid.Children.Add(rec);
                    }
                }
            }
        }

        private void Clear()
        {
            Rectangle rec;
            for (int y = 0; y < arrayHeightY; y++)
            {
                for (int x = 0; x < arrayWidthX; x++)
                {
                    if (arrayRec[x, y] != null)
                    {
                        rec = arrayRec[x, y];
                        stackPanelGrid.Children.Remove(rec);
                    }
                }
            }
            stackPanelGrid.Children.Clear();
        }



        private void Next_Click(object sender, EventArgs e)
        {

            PathFinding update = new PathFinding(this,arrayRec,arrayWidthX,arrayHeightY);

            //UpDateUIByRowsRight();
            //UpDateUIBySpirals();
            Thread loadThreads = new Thread(UpDateUIBySpirals);
            loadThreads.Start();
        }

        public void UpDateUIByRowsRight()
        {
            int x = 0;
            int y = 0;
            Boolean moveRight = true;
            Boolean mowableAreaFound = true;
            while (mowableAreaFound)
            {
                rows = arrayRec;
                rec = rows[x, y];
                
                SolidColorBrush rectBrush = (SolidColorBrush)rec.Fill;
                int tempx = x;
                int tempy = y;
                if (moveRight)
                {
                    if (rectBrush.Color == green.Color)//check current node
                    {
                        rows[x, y].Fill = yellow;//set current node
                        NodeCountRows += 1;
                        //rec.Fill = yellow;
                        tempx++;
                        if (tempx < arrayWidthX)//go to next node
                        {
                            x = tempx;
                        }
                        else//move down
                        {
                            tempy++;
                            if (tempy < arrayHeightY)
                            {
                                y++;
                                turnCountRows += 2;
                                NodeCountRows += 1;
                                moveRight = false;
                            }
                        }
                    }
                }
                else//move left
                {
                    if (rectBrush.Color == green.Color)
                    {
                        rows[x, y].Fill = yellow;
                        //rec.Fill = yellow;
                        NodeCountRows += 1;
                        tempx--;
                        if (tempx >= 0)
                        {
                            x = tempx;
                        }
                        else
                        {
                            tempy++;
                            if (tempy < arrayHeightY)
                            {
                                y++;
                                turnCountRows += 2;
                                NodeCountRows += 1;
                                moveRight = true;
                            }
                        }
                    }
                }
                for (int i = 0; i < arrayHeightY; i++)
                {
                    for (int j = 0; j < arrayWidthX; j++)
                    {
                        //if (rows[i, j] != null)
                        {
                            rec = arrayRec[i, j];

                            SolidColorBrush brush = (SolidColorBrush)rec.Fill;
                            if (brush.Color == Colors.Green)
                            {
                                mowableAreaFound = true;
                                break;
                            }
                            else
                                mowableAreaFound = false;
                        }
                    }
                    if (mowableAreaFound)
                        break;
                    else
                        mowableAreaFound = false;
                }
                
            }
        }


        public void UpDateUIByRowsLeft()
        {
            int x = 0;
            int y = 0;
            Boolean moveRight = false;
            Boolean mowableAreaFound = true;
            Boolean moveDown = true;
            while (mowableAreaFound)
            {
                rows = arrayRec;
                rec = rows[x, y];
                rec.Dispatcher.BeginInvoke(new Action(() =>
                {
                    SolidColorBrush rectBrush = (SolidColorBrush)rec.Fill;
                    int tempx = x;
                    int tempy = y;

                    if (moveDown)
                    {
                        if (rectBrush.Color == green.Color)//check current node
                        {
                            rows[x, y].Fill = yellow;//set current node
                            NodeCountRows += 1;
                            //rec.Fill = yellow;
                            tempy++;
                            if (tempy < arrayHeightY)//go to next node
                            {
                                y = tempy;
                            }
                            else//move right
                            {
                                tempx++;
                                if (tempx < arrayWidthX)
                                {
                                    x++;
                                    turnCountRows += 2;
                                    NodeCountRows += 1;
                                    moveRight = true;
                                }
                                else//go up
                                {
                                    y--;
                                    moveDown = false;
                                }
                            }
                        }
                    }
                    else//move up
                    {
                        if (rectBrush.Color == green.Color)
                        {
                            rows[x, y].Fill = yellow;
                            //rec.Fill = yellow;
                            NodeCountRows += 1;
                            tempy--;
                            if (tempy >= 0)
                            {
                                y = tempy;
                            }
                            else//go left
                            {
                                tempx--;
                                if (tempx < arrayWidthX)
                                {
                                    x--;
                                    turnCountRows += 2;
                                    NodeCountRows += 1;
                                    moveRight = false;
                                }
                                else//go down
                                {
                                    moveDown = true;
                                }
                            }
                        }
                    }
                    /*
                    for (int i = 0; i < arrayHeightY; i++)
                    {
                        for (int j = 0; j < arrayWidthX; j++)
                        {
                            //if (rows[i, j] != null)
                            {
                                rec = arrayRec[i, j];

                                SolidColorBrush brush = (SolidColorBrush)rec.Fill;
                                if (brush.Color == Colors.Green)
                                {
                                    mowableAreaFound = true;
                                    break;
                                }
                                else
                                    mowableAreaFound = false;
                            }
                        }
                        if (mowableAreaFound)
                            break;
                        else
                            mowableAreaFound = false;
                    }
                     * */
                }));
                Thread.Sleep(500);
            }
        }
        SolidColorBrush recColor;
        public void UpDateUIBySpirals()
        {
            int startx = 0;
            int starty = 0;
            Boolean finishedCircling = false;
            Boolean goDown = false;
            Boolean goRight = true;
            Boolean moveVertical = true;
            //Boolean hitObstical = false;
            int x = startx;
            int y = starty;
            Boolean mowableAreaFound = true;
            while (!finishedCircling)
            {
                spirals = arrayRec;
                rec = arrayRec[x, y];
                rec.Dispatcher.BeginInvoke(new Action(() =>
                {
                    int tempx = x;
                    int tempy = y;

                    rec.Fill = yellow;//set current node yellow
                    if (moveVertical)
                    {
                        if (goRight == true)
                        {
                            tempx++;//check next rectangle
                            if (tempx < arrayWidthX)//check right bounds
                            {
                                recColor = (SolidColorBrush)arrayRec[tempx, tempy].Fill;
                                Color color = recColor.Color;
                                /*
                                if (color == red.Color)//next place is impassable
                                {
                                    hitObstical = true;
                                    tempx = x;
                                    if (arrayRec[tempx, tempy++].Fill != red && arrayRec[tempx, tempy] != null)
                                    {
                                        x = tempx;
                                        y = tempy;
                                    }
                                    else if (arrayRec[tempx, tempy - 2].Fill != red)
                                    {
                                        x = tempx;
                                        y = tempy;
                                    }
                                }
                                 * */
                                if(color == yellow.Color)
                                {
                                    y++;
                                    rec.Fill = yellow;
                                    moveVertical = false;
                                    goDown = true;
                                }
                                else//color is green
                                {
                                    x++;
                                    //hitObstical = false;
                                    rec.Fill = yellow;
                                    //moveVertical = false;
                                }
                                
                            }
                            else
                            {
                                goDown = true;
                                moveVertical = false;
                            }
                        }
                        else//go left
                        {
                            tempx--;//check next rectangle
                            if (tempx >= 0)//check right bounds
                            {
                                if(arrayRec[tempx,tempy].Fill == yellow)
                                {
                                    rec.Fill = yellow;
                                    moveVertical = false;
                                    goDown = false;
                                }
                                else
                                {
                                    rec.Fill = yellow;
                                    x--;
                                }
                            }
                            else
                            {
                                goDown = false;
                                moveVertical = false;
                            }
                        }
                    }
                    else//go up or down
                    {
                        if (goDown)//go down
                        {
                            tempy++;//check next rectangle
                            if (tempy < arrayHeightY)//check right bounds
                            {
                                rec.Fill = yellow;
                               
                                if (arrayRec[tempx, tempy].Fill == yellow)
                                {
                                    moveVertical = true;
                                    goRight = false;
                                }
                                else
                                {
                                    y++;
                                }
                            }
                            else//next node is either a yellow or off the map
                            {
                                goRight = false;
                                moveVertical = true;
                            }
                        }
                        else//go up
                        {
                            tempy--;
                            rec.Fill = yellow;
                            if (tempy >= 0)//check right bounds
                            {
                                if(arrayRec[tempx,tempy].Fill == yellow)
                                {
                                    moveVertical = true;
                                    goRight = true;
                                }
                                else
                                {
                                    y--;
                                }
                            }
                            else
                            {
                                moveVertical = true;
                                goRight = true;
                            }
                        }
                    }
                    for (int i = 0; i < arrayHeightY; i++)
                    {
                        for (int j = 0; j < arrayWidthX; j++)
                        {
                            //if (rows[i, j] != null)
                            {
                                rec = spirals[i, j];

                                SolidColorBrush brush = (SolidColorBrush)rec.Fill;
                                if (brush.Color == Colors.Green)
                                {
                                    mowableAreaFound = true;
                                    break;
                                }
                                else
                                    mowableAreaFound = false;
                            }
                        }
                        if (mowableAreaFound)
                            break;
                        else
                            mowableAreaFound = false;
                    }
                    
                }));
                Thread.Sleep(500);
            }
        }
    }
}