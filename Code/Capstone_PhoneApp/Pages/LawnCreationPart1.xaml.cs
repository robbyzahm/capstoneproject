﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using Capstone_PhoneApp.ViewModels;

namespace Capstone_PhoneApp.Pages
{
    public partial class LawnCreationPart1 : PhoneApplicationPage
    {
        DimensionsViewModel viewModel;
        private int lawnHeight =0;
        private int lawnWidth =0;
        private int mowerWidth =0;
        public LawnCreationPart1()
        {
            InitializeComponent();
            String lWidth = LawnWidthBox.Text;
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            if (LawnWidthBox.Text != null && LawnHeighthBox.Text != null)
            {
                this.lawnWidth = int.Parse(LawnWidthBox.Text);
                this.lawnHeight = int.Parse(LawnHeighthBox.Text);
            }
            if (mowerWidth <= lawnWidth)
            {
                viewModel = new DimensionsViewModel(lawnHeight, lawnWidth, mowerWidth);
                if (viewModel != null)
                {
                    PhoneApplicationService.Current.State["yourparam"] = viewModel;
                }
                NavigationService.Navigate(new Uri("/Pages/LawnCreationPart2.xaml", UriKind.Relative));
                //String Text = (String)LawnWidthBox.DataContext;
            }
        }
    }
}