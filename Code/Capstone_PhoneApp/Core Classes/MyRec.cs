﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace Capstone_PhoneApp.Core_Classes
{
    class MyRec : INotifyPropertyChanged
    {
        private Rectangle _rec;

        // Declare the PropertyChanged event.
        public event PropertyChangedEventHandler PropertyChanged;

        // Create the property that will be the source of the binding.
        public Rectangle color1
        {
            get { return _rec; }
            set
            {
                _rec = value;
                NotifyPropertyChanged("rec");
            }
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
