﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Shapes;
using System.Windows.Media;
using Capstone_PhoneApp.Pages;

namespace Capstone_PhoneApp.Core_Classes
{
    public class Square
    {
        private OpenLawn parent;
        private Rectangle squareRect;
        private int row, column;
        private bool highlight;

        public Square(OpenLawn parent, int row, int column)
        {
            this.squareRect = new Rectangle();
            this.squareRect.Fill = ((row + column) % 2) == 0 ? new SolidColorBrush(Color.FromArgb(255, 193, 124, 66)) : new SolidColorBrush(Color.FromArgb(255, 231, 208, 167));
            this.UpdateHighlight();
            Grid.SetRow(this.squareRect, row);
            Grid.SetColumn(this.squareRect, column);
            this.parent = parent;
            //this.parent.LayoutRoot.Children.Add(this.squareRect);
            this.row = row;
            this.column = column;
        }

        public bool Highlight
        {
            get { return this.highlight; }
            set
            {
                this.highlight = value;
                this.UpdateHighlight();
            }
        }
        private void UpdateHighlight()
        {
            if (this.highlight)
            {
                this.squareRect.StrokeThickness = 10;
                this.squareRect.Stroke = new SolidColorBrush(Colors.Black);
            }
            else
            {
                this.squareRect.StrokeThickness = 0.5;
                this.squareRect.Stroke = new SolidColorBrush(Colors.Black);
            }
        } 
    }
}
