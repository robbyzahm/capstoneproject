﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace Capstone_PhoneApp.PathEngine
{
    public class Movement
    {
        private Rectangle[,] rec;
        public Movement(Rectangle[,] rec)
        {
            this.rec = rec;
        }

        public PathData Noth(PathData data)
        {
            PathData tempdata = data;
            tempdata.y--;
            return tempdata;
        }

        public PathData East(PathData data)
        {
            PathData tempdata = data;
            tempdata.x++;
            return tempdata;
        }

        public PathData West(PathData data)
        {
            PathData tempdata = data;
            tempdata.x--;
            return tempdata; 
        }

        public PathData South(PathData data)
        {
            PathData tempdata = data;
            tempdata.y++;
            return tempdata;
        }
    }
}
