﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capstone_PhoneApp.PathEngine
{
    public class Path
    {
        private int currentX;
        private int currentY;
        private int[,] grid;
        private int row;
        private int col;
        public Path(int startX, int startY, int[,] grid, int gridSizeX, int gridSizeY)
        {
            this.currentX = startX;
            this.currentY = startY;
            this.grid = grid;
            this.row = gridSizeY;
            this.col = gridSizeX;
        }
         //finds a path for mowing the lawn
        public void FindPath() //y = row, x = col
        {
            Boolean TraverseDown = true;
            while(IsLawnCovered())
            {
                if(grid[currentX,currentY] == 1)//first mowable area
                {
                    if (TraverseDown)
                    {
                        MoveDown();
                        TraverseDown = false;
                    }
                    else
                    {
                        MoveUp();
                        TraverseDown = true;
                    }
                }
                else
                {
                    FindMowableArea();
                }
            }
        }

        public void MoveDown()
        {
            int tempY = currentY;
            while(currentY < row)
            {
                tempY++;
                if(tempY < row)
                {
                    if (!CheckForObstical(currentX, tempY))
                    {
                        currentY++;
                        Console.WriteLine("X:" + currentX + "Y:"+currentY);
                        grid[currentX, currentY] = 3;
                    }
                    else
                    {
                        FindMowableArea();
                    }
                }
                else
                {
                    if(!CheckForObstical(currentX,currentY))
                    {
                        currentX++;
                        Console.WriteLine("X:" + currentX + "Y:" + currentY);
                        grid[currentX, currentY] = 3;
                    }
                    else
                    {
                        FindMowableArea();
                    }
                }
            }
        }

        public void MoveUp()
        {
            int tempY = currentY;
            while (tempY > 0)
            {
                if (tempY-- < row)
                {
                    if (!CheckForObstical(currentX,tempY))
                    {
                        currentY--;
                        Console.WriteLine("X:" + currentX + "Y:" + currentY);
                        grid[currentX, currentY] = 3;
                    }
                    else
                    {
                        FindMowableArea();
                    }
                }
                else
                {
                    if(!CheckForObstical(currentX,tempY))
                    {
                        currentX++;
                        Console.WriteLine("X:" + currentX + "Y:" + currentY);
                        grid[currentX, currentY] = 3;
                    }
                    else
                    {
                        FindMowableArea();
                    }
                }
            }
        }

        public bool CheckForObstical(int x, int y)
        {
            Boolean foundObstical = true;
            if(grid[x,y] == 1)
            {
                foundObstical = false;
            }
            return foundObstical;
        }
        
        //checks to see what side is the longest
        public Boolean LongestSide()
        {
            Boolean rowIsLongest = true;
            if (row < col)
            {
                rowIsLongest = false;
            }
            return rowIsLongest;
        }
        
        //this method is for when you need to mow diagonally
        public void MoveDiagonally(String direction)
        {//northeast, northwest, southeast, southwest
            
        }
        
        //this method is for when you need to mow around an obstical
        public void CircleObstacles()
        {
           for(int i = 0; i < row; i++)
           {
                for(int j = 0; j< col; j++)
                {
                    if(grid[i,j] ==0)//check sides of the obstical and mow around it
                    {
                        //check bottom, top, left, right, four diagnal corners
                    }
                }
           }
        }
        
        //check to see if everything on the lawn has been mowed
        public bool IsLawnCovered()
        {
            Boolean foundMowableArea = false;
            for(int i = 0; i < row;i++)
            {
                for(int j = 0; j < col;j++)
                {
                    if(grid[i,j] == 1)
                    {
                        foundMowableArea = true;
                        break;
                    }
                }
            }
            return foundMowableArea;
        }

        public void TravelHorizontally()
        {
            currentX++;
        }

        public void TravelVertically()
        {
            currentY++;
        }

        public void FindMowableArea()//check for the next available mowable area
        {
            int tempX = currentX;
            int tempY = currentY;
            if(grid[tempX++, tempY++] == 1)//southeast
            {
                currentX = tempX;
                currentY = tempX;
            }
            else if (grid[tempX--, tempY++] == 1)//southwest
            {
                currentX = tempX;
                currentY = tempX;
            }
            else if (grid[tempX--, tempY--] == 1)//northwest
            {
                currentX = tempX;
                currentY = tempX;
            }
            else if(grid[tempX++,tempY--] == 1)//northeast
            {
                currentX = tempX;
                currentY = tempX;
            }
            else if (grid[tempX++, tempY] == 1)//east
            {
                currentX = tempX;
                currentY = tempX;
            }
            else if (grid[tempX, tempY++] == 1)//south
            {
                currentX = tempX;
                currentY = tempX;
            }
            else if (grid[tempX--, tempY] == 1)//west
            {
                currentX = tempX;
                currentY = tempX;
            }
            else if (grid[tempX, tempY--] == 1)//north
            {
                currentX = tempX;
                currentY = tempX;
            }
            else //something went wrong start over
            {
                currentX = 0;
                currentY = 0;
            }
        }

        public void Move()
        {
            if(CheckIfOutOfBounds() == -1)//went out right side of grid
            {
                currentX--;
                currentY++;
            }
            else if(CheckIfOutOfBounds() == 0)
            {
                
            }
            else if(CheckIfOutOfBounds() == 1)
            {
                
            }
            else if(CheckIfOutOfBounds() == 2)
            {
                
            }
            else //3
            {
                
            }
        }

        public int CheckIfOutOfBounds()
        {// codes -1 = in bounds,0 = north,1 = west,2 = south,3 = east
            int outOfBoundsCode = 0;
            if(currentX < row || currentX >= 0 && currentY < col && col >= 0)// in bounds
            {
                outOfBoundsCode = -1;
            }
            else if (currentX > row || currentX < 0 && currentY < col && col >= 0)//check east
            {
                outOfBoundsCode = 2;
            }
            else if(currentX < row && currentX >= 0 && currentY > col || col >= 0)//check south
            {
                outOfBoundsCode = 3;
            }
            else if(currentX < row && currentX >= 0 && currentY < col && col >= 0)// check west
            {
                outOfBoundsCode = 1;
            }
            else//north
            {
                outOfBoundsCode = 0;
            }
            return outOfBoundsCode;
        }

        public void oldMove(int startx, int starty)
        {
            //Boolean travelReverse = false ;
            while (IsLawnCovered() == false)
            {
                //if (grid[startX, startY] == 1)
                {
                    if (LongestSide())//start moving across the x axis of the lawn
                    {
                        if (currentY < row && currentX < col)// true if x and y are in bounds
                        {
                            if (CheckForObstical(currentX, currentY))//current area is an obstacle
                            {
                                FindMowableArea();
                            }
                            else//area is not an obstacle so mark it as mowed
                            {
                                grid[currentX, currentY] = 3;
                                currentY++;
                            }
                        }
                        else// both x and y are out of bounds
                        {//
                            if (IsLawnCovered())
                                Console.WriteLine("Lawn is finished");//later trigger event
                            else
                            {
                                //startOver
                                Console.WriteLine("Program did not find a path");//later trigger events
                            }
                        }
                    }
                }
            }
        }
    }
}
