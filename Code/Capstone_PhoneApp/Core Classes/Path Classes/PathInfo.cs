﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capstone_PhoneApp.PathEngine
{
    public class PathData
    {
        public int x { get; set; }
        public int y { get; set; }
        public PathData(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
