﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capstone_PhoneApp.Core_Classes.Path_Classes
{
    public class Main
    {
        //codes
        // 0 = obstacle
        // 1 = mowable but not mowed
        // 3 = area mowed
        private int row = 10;
        private int col = 10;
        private int[,] grid;
        

        public Main()
        {
            //Console.WriteLine("how wide is the mower");
            //String mowerWidth = Console.ReadLine();
            MakeGrid();
            AddObsticales();
        }

        public void MakeGrid()
        {
            grid = new int[col, row];
            for (int x = 0; x < col; x++)
            {
                for (int y = 0; y < row; y++)
                {
                    grid[x, y] = 1;
                }
            }
        }

        public void AddObsticales()
        { // 0,7 0,8} 2,3 2,4 2,5 2,6} 3,3 3,4 3,5 3,6} 4,3, 4,4 4,5 4,6} 5,3 5,4 5,5 5,6} 6,1 6,2 6,4} 7,1 7,2 7,4 7,7 7,8} 8,4 8,7 8,8} 9,4

            grid[0, 7] = 0;
            grid[0, 8] = 0;

            grid[2, 3] = 0;
            grid[2, 4] = 0;
            grid[2, 5] = 0;
            grid[2, 6] = 0;

            grid[3, 3] = 0;
            grid[3, 4] = 0;
            grid[3, 5] = 0;
            grid[3, 6] = 0;

            grid[4, 3] = 0;
            grid[4, 4] = 0;
            grid[4, 5] = 0;
            grid[4, 6] = 0;

            grid[5, 3] = 0;
            grid[5, 4] = 0;
            grid[5, 5] = 0;
            grid[5, 6] = 0;

            grid[7, 1] = 0;
            grid[7, 2] = 0;
            grid[7, 4] = 0;
            grid[7, 7] = 0;
            grid[7, 8] = 0;

            grid[8, 4] = 0;
            grid[8, 7] = 0;
            grid[8, 8] = 0;

            grid[9, 4] = 0;
        }

        public void FindPath()
        {
            int startX = 0;
            int startY = 0;
            Boolean reachedEnd = true;
            while(!reachedEnd)
            {
                if(grid[startX,startY] == 1)
                {
                    if(RowIsLongest())//start moving across the x axis of the lawn
                    {
                        for(int y = startX; y < row;y++)
                        {
                            int coveredY = y;
                            for(int x = startX; x < col;x++)
                            {
                                int coveredX = x;
                                if(grid[coveredX,coveredY] ==0)//current area is an obstacle
                                {
                                    y++;
                                }
                                else//area is not an obstacle so mark it as mowed
                                {
                                    grid[coveredX, coveredY] = 3;
                                }
                            }
                        }
                    }
                    else//move across the y axis of the lawn
                    {
                        for (int x = startX; x < col; x++)
                        {
                            int coveredX = x;
                            for (int y = startY; y < row; y++)
                            {
                                int coveredY = y;
                                if (grid[coveredX, coveredY] == 0)//current area is an obstacle
                                {
                                    y++;
                                }
                                else//area is not an obstacle so mark it as mowed
                                {
                                    grid[coveredX, coveredY] = 3;
                                }
                            }
                        }
                    }
                }
            }   
        }

        public Boolean RowIsLongest()
        {
            Boolean rowIsLongest = true;
            if (row < col)
            {
                rowIsLongest = false;
            }
            return rowIsLongest;
        }

        public void TraverseLawn()
        {
            
        }

        public void MoveDiagonally()
        {
            
        }

        public void CircleObstacles()
        {
           
        }
    }
}
