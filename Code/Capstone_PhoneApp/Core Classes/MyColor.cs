﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Capstone_PhoneApp.Core_Classes
{
    // Create a class that implements INotifyPropertyChanged.
    public class MyColors : INotifyPropertyChanged
    {
        private SolidColorBrush _color1;

        // Declare the PropertyChanged event.
        public event PropertyChangedEventHandler PropertyChanged;

        // Create the property that will be the source of the binding.
        public SolidColorBrush color1
        {
            get { return _color1; }
            set
            {
                _color1 = value;
                NotifyPropertyChanged("color1");
            }
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
