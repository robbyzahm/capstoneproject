﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capstone_PhoneApp.ViewModels
{
    public class DimensionsViewModel
    {
        public int lawnHeight{ get; set; }
        public int lawnWidth{ get; set; }
        public int mowerWidth{ get; set; }

        public DimensionsViewModel(int lawnHeight, int lawnWidth, int mowerWidth)
        {
            this.lawnHeight = lawnHeight;
            this.lawnWidth = lawnWidth;
            this.mowerWidth = mowerWidth;
        }
    }
}
